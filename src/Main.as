package
{
    
    import flash.display.Sprite;
    import com.holloran.Application.App;
    
    [SWF(width="800", height="600", backgroundColor="0xFFFFFF", frameRate="24" )]
    public class Main extends Sprite
    {
        
        public function Main ()
        {
            super();
            
            //Creates a new weather app
            var app:App = new App();
            app.x = stage.stageWidth/2;
            app.y = stage.stageHeight/2;
            this.addChild(app);
        
        }
    
    }

}
