package com.holloran.vo
{
    
    public class WeatherVO
    {
        public var date:String;
        
        public var invalid:String;
        
        public var cityName:String;
        
        public var stateName:String
        
        public var todaysHigh:String;
        
        public var todaysLow:String;
        
        public var speedUnits:String;
        
        public var tempUnits:String;
        
        public var location:String;
        
        public var currentTemp:String;
        
        public var tomorrowsHigh:String;
        
        public var tomorrowsLow:String;
        
        public var windDirection:String;
        
        public var windSpeed:String;
        
        public var weatherCode:String;
        
        public function WeatherVO ()
        {
        }
    }
}