package com.holloran.utils
{
    
    public class StringUtils
    {
        public function StringUtils ()
        {
        }
        
        /**
         * Used to truncate a string at a chosen character.
         *
         * @param value Is the string that the user enters that they want to be truncated.
         * @param length Is the max amount of characters the user wants before the string is truncated.
         * @return This will return the truncated string back to the user
         *
         * @example <code>
         * var value:String = tfString.text;
         *	var length:int = int(tfLength.text);
         *	var trunc:String = StringUtils.truncate(value,length);
         *	tfTrunc.text = trunc;
         * </code>
         *
         */
        public static function truncate (value:String, length:int):String
        {
            var trunc:String = value;
            
            if ( trunc.length>length )
            {
                trunc=trunc.substr(0,length);
                
            }
            return trunc
        
        }
        
        public static function CalcWindDirection (d:String):String
        {
            //Change the degrees to compass directions
            /*		n
            nw   ne
            w         e
            sw	  se
            s*/
            
            var direction:String;
            var wd:int = int(d);
            
            if ( wd>=0 && wd<=45 )
            {
                //north
                direction = "N"
            }
            else if ( wd>=46 && wd<=90 )
            {
                //north east
                direction = "NE"
            }
            else if ( wd>=91 && wd<=135 )
            {
                //east
                direction = "E"
            }
            else if ( wd>=136 && wd<=180 )
            {
                //south east
                direction = "SE"
            }
            else if ( wd>=181 && wd<=225 )
            {
                //south
                direction = "S"
            }
            else if ( wd>=226 && wd<=270 )
            {
                //south west
                direction = "SW"
            }
            else if ( wd>=271 && wd<=315 )
            {
                //west
                direction = "W"
            }
            else if ( wd>=316 && wd<=360 )
            {
                //north west
                direction = "NW"
            }
            
            return direction
        
        }
    }
}