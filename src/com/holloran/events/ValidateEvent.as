package com.holloran.events
{
    
    import flash.events.Event;
    
    public class ValidateEvent extends Event
    {
        public static const VALID:String = "valid";
        
        //used to set the weather apps zip code
        public var zipCode:String;
        
        public function ValidateEvent (type:String, bubbles:Boolean=false, cancelable:Boolean=false)
        {
            super(type, bubbles, cancelable);
        
        }
        
        override public function clone ():Event
        {
            return new ValidateEvent( type, bubbles, cancelable );
        
        }
    }
}