package com.holloran.events
{
    
    import flash.events.Event;
    
    public class ResetEvent extends Event
    {
        public static const RESET:String = "reset";
        
        public function ResetEvent (type:String, bubbles:Boolean=false, cancelable:Boolean=false)
        {
            super(type, bubbles, cancelable);
        
        }
        
        override public function clone ():Event
        {
            return new ResetEvent( type, bubbles, cancelable );
        
        }
    }
}