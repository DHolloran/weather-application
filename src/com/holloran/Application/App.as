package com.holloran.Application
{
    
    //weather rss "http://weather.yahooapis.com/forecastrss?p="32817"
    //yahoo developer page for weather api: http://developer.yahoo.com/weather/
    
    import com.holloran.events.ResetEvent;
    import com.holloran.events.ValidateEvent;
    import com.holloran.views.LocInputView;
    import com.holloran.views.WeatherAppView;
    import flash.display.Sprite;
    import libs.WeatherAppBase;
    
    [SWF(width="800", height="600", backgroundColor="0x00CCFF", frameRate="24" )]
    public class App extends Sprite
    {
        private var _locInput:LocInputView;
        
        private var _areaCode:String;
        
        private var _weatherApp:WeatherAppView;
        
        
        public function App ()
        {
            super();
            createLocInput();
        
        }
        
        private function createLocInput ():void
        {
            _locInput = new LocInputView();
            _locInput.x = this.width/2 - (_locInput.width/2);
            _locInput.y = this.height/2 - (_locInput.height/2);
            this.addChild(_locInput);
            _locInput.addEventListener(ValidateEvent.VALID, onValid);
        
        }
        
        private function createWeatherApp ():void
        {
            _weatherApp = new WeatherAppView(_areaCode);
            _weatherApp.x = this.width/2 - (_weatherApp.width/2);
            _weatherApp.y = this.width/2 - (_weatherApp.height/2);
            this.addChild(_weatherApp);
            _weatherApp.addEventListener(ResetEvent.RESET, onReset);
        
        }
        
        private function onValid (evt:ValidateEvent):void
        {
            var loc:String = evt.zipCode;
            _areaCode = loc;
            this.removeChild(_locInput)
            createWeatherApp();
        
        }
        
        private function onReset (evt:ResetEvent):void
        {
            this.removeChild(_weatherApp);
            createLocInput();
        
        }
    
    
    }

}
