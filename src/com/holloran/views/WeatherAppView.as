package com.holloran.views
{
    
    import com.holloran.events.ImageEvent;
    import com.holloran.events.ResetEvent;
    import com.holloran.loaders.ImageLoader;
    import com.holloran.utils.StringUtils;
    import com.holloran.vo.WeatherVO;
    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import libs.WeatherAppBase;
    
    public class WeatherAppView extends WeatherAppBase
    {
        
        private var _date:String;
        
        private var _direction:String;
        
        private var _ld:ImageLoader;
        
        private var _vo:WeatherVO;
        
        public function WeatherAppView (loc:String)
        {
            super();
            
            _vo = new WeatherVO();
            setup();
            
            //Starts the loading of the xml file
            var url:String = "http://weather.yahooapis.com/forecastrss?p=" + loc;
            var ul:URLLoader = new URLLoader();
            ul.load(new URLRequest(url));
            ul.addEventListener(Event.COMPLETE, onParse);
        
        }
        
        private function setup ():void
        {
            //Sets default values
            this.tfCityName.text = ""
            this.tfCurrentTemp.text = "";
            this.tfCurrentWindSpeed.text = "";
            this.tfCurrentWindDirection.text = "";
            this.tfDate.text = "";
            this.tfTodayHighTemp.text = "";
            this.tfTodayLowTemp.text = "";
            this.tfTomorrowsHighTemp.text = "";
            this.tfTomorrowsLowTemp.text = "";
            this.submitChangeBTN.mouseChildren = false;
            this.submitChangeBTN.buttonMode = true;
            this.submitChangeBTN.addEventListener(MouseEvent.CLICK, onClick);
        
        }
        
        private function onParse (e:Event):void
        {
            var xmlData:XML = XML(e.target.data);
            var ns1:Namespace = new Namespace("http://xml.weather.yahoo.com/ns/rss/1.0");
            
            _vo.invalid = xmlData.channel.title;
            
            //Validates a valid yahoo weather area code
            if ( _vo.invalid == "Yahoo! Weather - Error" )
            {
                this.tfCityName.text = "Invalid area code"
                this.tfCurrentTemp.text = "N/A";
                this.tfCurrentWindSpeed.text = "N/A";
                this.tfCurrentWindDirection.text = "";
                this.tfDate.text = "N/A";
                this.tfTodayHighTemp.text = "N/A";
                this.tfTodayLowTemp.text = "N/A";
                this.tfTomorrowsHighTemp.text = "N/A";
                this.tfTomorrowsLowTemp.text = "N/A";
            }
            else
            {
                //Sets all WeatherVO properties = to corresponding xml
                _vo.cityName = xmlData.channel.ns1::location.@city; //City
                _vo.stateName = xmlData.channel.ns1::location.@region; //State
                _vo.todaysHigh = xmlData.channel.item.ns1::forecast[0].@high; //Todays High
                _vo.todaysLow = xmlData.channel.item.ns1::forecast[0].@low; //Todays Low
                _vo.tomorrowsHigh = xmlData.channel.item.ns1::forecast[1].@high; //Tomorrows High
                _vo.tomorrowsLow = xmlData.channel.item.ns1::forecast[1].@low; //Tomorrows Low
                _vo.date = xmlData.channel.item.ns1::condition.@date; //Date
                _vo.currentTemp = xmlData.channel.item.ns1::condition.@temp; //Current Temperature
                _vo.windSpeed = xmlData.channel.ns1::wind.@speed; //Wind Speed
                _vo.windDirection = xmlData.channel.ns1::wind.@direction; //Wind Direction
                _vo.weatherCode = xmlData.channel.item.ns1::condition.@code; //Condition Code
                _vo.speedUnits = xmlData.channel.ns1::units.@speed; //Speed mph/kmh
                _vo.tempUnits = xmlData.channel.ns1::units.@temperature; //Temperature F/C
                
                init();
            }
        
        }
        
        private function init ():void
        {
            calcWindDirection()
            truncDate();
            imageLoader();
            
            this.tfDate.text = _date; //Date
            this.tfCityName.text = _vo.cityName + ", " + _vo.stateName; //City, State
            this.tfCurrentTemp.text = _vo.currentTemp +"°" + _vo.tempUnits; //Current Temperature + F/C
            this.tfTodayHighTemp.text = _vo.todaysHigh + "°" + _vo.tempUnits; //Today's High Temperature + F/C
            this.tfTodayLowTemp.text = _vo.todaysLow + "°" + _vo.tempUnits; //Today's Low Temperature + F/C
            this.tfTomorrowsHighTemp.text = _vo.tomorrowsHigh + "°" + _vo.tempUnits; //Tomorrows High Temperature + F/C
            this.tfTomorrowsLowTemp.text = _vo.tomorrowsLow + "°" + _vo.tempUnits; //Tomorrows low Temperature + F/C
            this.tfCurrentWindSpeed.text = _vo.windSpeed + _vo.speedUnits + " "; //Wind Speed mph/kmh 
            this.tfCurrentWindDirection.text = _direction; //Wind Direction
        
        
        }
        
        private function imageLoader ():void
        {
            var url:String = "assets/img/"; //Folder structure for url
            var img:String = _vo.weatherCode + ".png"; //Image for url
            
            //Creates a new image loader
            _ld = new ImageLoader(url+img);
            _ld.addEventListener(ImageEvent.IMAGE_LOADED, onLoad);
        
        }
        
        private function onLoad (e:ImageEvent):void
        {
            //situatesa and places the weather icon on the stage
            var image:DisplayObject = e.image;
            image.x = 530;
            image.y = 180;
            image.width = 160;
            image.height = 160;
            this.addChild(image);
        
        }
        
        private function onClick (evt:MouseEvent):void
        {
            //Resets to the LocInputView
            var e:ResetEvent = new ResetEvent(ResetEvent.RESET);
            this.dispatchEvent(e);
        
        }
        
        private function calcWindDirection ():void
        {
            //Converts the degrees to the compass direction
            _direction = StringUtils.CalcWindDirection(_vo.windDirection);
        
        }
        
        private function truncDate ():void
        {
            //Truncates the date value to remove the time
            var value:String = _vo.date;
            var length:int = int(16);
            _date = StringUtils.truncate(value,length);
        
        
        }
    }
}