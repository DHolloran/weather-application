package com.holloran.views
{
    
    import com.holloran.events.ValidateEvent;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import libs.LocInputBase;
    
    public class LocInputView extends LocInputBase
    {
        private var _location:String;
        
        public function LocInputView ()
        {
            super();
            setup();
        
        }
        
        private function setup ():void
        {
            //Sets the default values of the location input
            this.tfLocInput.text = "";
            this.tfErrorText.text = "";
            this.tfLocInput.maxChars = 5;
            this.tfLocInput.restrict = "0-9"
            this.submitBTN.mouseChildren = false;
            this.submitBTN.buttonMode = true;
            this.submitBTN.addEventListener(MouseEvent.CLICK, onClick);
        
        }
        
        private function validateTF ():void
        {
            //Validates the area code entered
            if ( tfLocInput.length == 5 )
            {
                //Dispatches ValidateEvent if area code is correct
                var e:ValidateEvent = new ValidateEvent(ValidateEvent.VALID);
                _location = this.tfLocInput.text;
                e.zipCode = _location
                this.dispatchEvent(e);
            }
            else
            {
                //Incorrect area code error text
                this.tfErrorText.text = "Please enter 5 digit area code."
            }
        
        }
        
        private function onClick (evt:Event):void
        {
            validateTF();
        
        }
    
    }
}